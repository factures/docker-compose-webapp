#!/bin/sh

docker build -t nibupac/factures-frontend https://framagit.org/factures/angular-frontend.git
docker build -t nibupac/factures-frontend:production https://framagit.org/factures/angular-frontend.git -f docker/production
docker build -t nibupac/factures-backend https://framagit.org/factures/node-backend.git
